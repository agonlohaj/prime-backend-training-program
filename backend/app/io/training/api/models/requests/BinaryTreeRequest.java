package io.training.api.models.requests;

import io.training.api.models.BinaryTree;
import lombok.Data;

/**
 * Created by agonlohaj on 28 Aug, 2020
 */
@Data
public class BinaryTreeRequest {
	int value;
	BinaryTree tree;
}
