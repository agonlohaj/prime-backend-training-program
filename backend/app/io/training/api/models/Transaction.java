package io.training.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Transaction extends BaseModel {
	private String transactionId;
	private String dateTime;
	private Integer channelCode;
	private Integer lineItemId;
	private Double volume;
	private Double salesIncVatActual;
	private Double priceIncVat;
	private Double priceIncVatOriginal;
	private Long upcCode;
	private String upcName;
	private String categoryName;
	private String subCategoryName;
	private String brandName;
	private String packageName;
	private String supplierName;
	private Integer numberOfTransactions;
	private String gender;
	private String customerSegment;
	private Integer terminalCheckRegister;
	private Integer zipCode;
	private String zipCodeExtend;
	private String zipCodeTotal;
	private Integer numOfEmployees;
	private Integer m2;
	private String storeSegment;
	private String region;
	private Double marginOctober;
	private Double priceIncVatOctober;
	private Double discountOctober;
}
