package io.training.api.services;

import com.google.inject.Inject;
import io.training.api.models.requests.BinarySearchRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.models.responses.ChartData;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;

/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class AssignemntTwoService {
    @Inject
    HttpExecutionContext ec;


    /**
     * Function as Line: y = x * 2
     * @param input
     * @return
     */
    public CompletableFuture<List<Integer>> function1 (List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            return new ArrayList<>();
        }, ec.current());
    }


    /**
     * Function as Scatter y = square root of the absolute value of ((x ^ 2) + (x * 4))
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function2 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {

            // TODO: do something smarter here...
            return new ArrayList<>();
        }, ec.current());
    }

    /**
     * Function y = If 3^2 - x^2 > 0 than square root of (3^2 - x^2). If 3^2 - x^2 < 0 then - square root of absolute value of (3^2 - x^2)
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function3 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {


            return new ArrayList<>();
        }, ec.current());
    }

    /**
     * Function as Line: y = sin(x)
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function4 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {


            return new ArrayList<>();
        }, ec.current());
    }

    /**
     * Function as Line: y = cos(x)
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function5 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return new ArrayList<>();
        }, ec.current());
    }

    /**
     * 2 Line Functions displayed together, one for Sin and one for Cos, check only the series option to include two of them
     * Returns two lists of double the first list for sin, and the second one for cos
     * @param input
     * @return
     */
    public CompletableFuture<List<List<Double>>> function6 (List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            List<List<Double>> result = new ArrayList<>();
            return result;
        }, ec.current());
    }

    /**
     * I want to see the top 4 performing words, given the randomCategoryData, the top 4 with the highest random generated value
     * Make sure the values are summed on repeated words (no duplicates)
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function7 (List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            try {
                return new ArrayList<>();
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CompletionException(ex);
            }
        }, ec.current());
    }

    /**
     * Calculate the average within the groups now, and show that here. Check the random Category data on how it generates those
     * @param input
     * @return
     */
    public CompletableFuture<List<ChartData>> function8 (List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return new ArrayList<>();
        }, ec.current());
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function9 (List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return new ArrayList<>();
        }, ec.current());
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     * @param request
     * @return index - the index of the search
     */
    public CompletableFuture<Integer> binarySearch (BinarySearchRequest request) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return -1;
        }, ec.current());
    }
}
