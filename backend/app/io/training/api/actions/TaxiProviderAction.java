package io.training.api.actions;

import io.training.api.models.Taxi;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

/**
 * Created by Agon on 09/08/2020
 */
public class TaxiProviderAction extends Action.Simple {

	@Override
    public CompletionStage<Result> call(Http.Request request) {
		request = request.addAttr(Attributes.TAXI_TYPED_KEY, new Taxi());
		return delegate.call(request);
    }
}
