package io.training.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Strings;
import io.training.api.models.validators.HibernateValidator;
import io.training.api.models.Rental;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class ValidationController extends Controller {

	@BodyParser.Of(BodyParser.Json.class)
	public Result save (Http.Request request) {
		JsonNode node = request.body().asJson();

		Rental car = Json.fromJson(node, Rental.class);
		String errors = HibernateValidator.validate(car);
		if (!Strings.isNullOrEmpty(errors)) {
			return badRequest(errors);
		}
		return ok(Json.toJson(car));
	}

//	@BodyParser.Of(BodyParser.Json.class)
//	public Result save (Http.Request request) {
//		JsonNode node = request.body().asJson();
//
//		Car car = Json.fromJson(node, Car.class);
//		if (Strings.isNullOrEmpty(car.getManufacturer())) {
//			return badRequest("Manufacturer cannot be empty!");
//		}
//		if (Strings.isNullOrEmpty(car.getLicensePlate())) {
//			return badRequest("License Place cannot be empty!");
//		}
//		if (car.getLicensePlate().length() < 2 || car.getLicensePlate().length() > 14) {
//			return badRequest("License Place should be between 2 and 14!");
//		}
//		if (car.getSeatCount() < 2 || car.getSeatCount() > 100) {
//			return badRequest("Seat count cannot be less than 2!");
//		}
//		if (!car.isUpAndRunning()) {
//			return badRequest("The car should be up and running!");
//		}
//		if (car.getTags().size() == 0) {
//			return badRequest("There should be at least 1 tag!");
//		}
//		return ok(Json.toJson(car));
//	}

}