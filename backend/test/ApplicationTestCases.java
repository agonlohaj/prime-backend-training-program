import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.NOT_FOUND;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.route;
import java.util.List;
import com.fasterxml.jackson.databind.JsonNode;

import org.junit.Before;
import org.junit.Test;

import io.training.api.models.Taxi;
import io.training.api.utils.DatabaseUtils;
import play.Logger;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;

public class ApplicationTestCases extends WithApplication {

	@Test
	public void testIndex() {
		final Http.RequestBuilder homeRequest = new Http.RequestBuilder().method("GET").uri("/");
		final Result result = route(app, homeRequest);
		assertEquals(OK, result.status());
		assertEquals("text/html", result.contentType().get());
		assertEquals("utf-8", result.charset().get());
		assertTrue(contentAsString(result).contains("Welcome to your Play Web Application!"));
	}

	@Test
	public void testMongo() {
		final Http.RequestBuilder homeRequest = new Http.RequestBuilder().method("GET").uri("/mongo");
		final Result result = route(app, homeRequest);
		assertEquals("application/json", result.contentType().get());
		assertEquals(OK, result.status());

		JsonNode body = Json.parse(contentAsString(result));
		List<Taxi> taxis = DatabaseUtils.parseJsonListOfType(body, Taxi.class);
		assertEquals("Expected the collection to have no elements!", taxis.size(), 0);
	}
}
