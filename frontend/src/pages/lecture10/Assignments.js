/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import Code from "presentations/Code";
import SimpleLink from "presentations/rows/SimpleLink";
import faker from 'faker'
import withTests from 'middleware/withTests'
import LoadingIndicator from "presentations/LoadingIndicator";
import ErrorBox from "presentations/ErrorBox";
import Chart from "presentations/Chart";
import { Highlighted } from "presentations/Label";
import { Button } from "@material-ui/core";

const styles = ({ size, palette, typography }) => ({
  root: {},
  graphs: {
    display: 'flex',
    flexFlow: 'row wrap',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    width: '100%'
  },
  title: {
    zIndex: 10,
  },
  card: {
    backgroundColor: 'white',
    width: `calc(32% - ${size.spacing * 2}px)`,
    margin: size.spacing,
    height: 420,
    padding: 8,
    display: 'flex',
    flexFlow: 'column wrap',
    position: 'relative',
    alignItems: 'flex-start'
  },
  graph: {
    display: 'flex',
    flex: 1,
    width: '100%',
    height: 'auto'
  },
  copy: {
    position: 'absolute',
    bottom: size.spacing,
    right: size.spacing,
    color: palette.leadColor,
    zIndex: 500
  }
})

const provided = `POST           /api/lecture9/ingest                  @io.training.api.controllers.LectureNineController.ingest(request: Request)`

const paginationRoutes = `GET            /api/assignments/lecture10/paginated                                      @io.training.api.controllers.AssignmentLectureTenController.paginated(request: Request, limit: Integer ?= 50, skip: Integer ?= 0, String token ?= "")
GET            /api/assignments/lecture10/subscribe                                      @io.training.api.controllers.AssignmentLectureTenController.subscribe(request: Request)`

const graphRoutes = `GET            /api/assignments/lecture10/assignment1                                    @io.training.api.controllers.AssignmentLectureTenController.assignment1(request: Request)
GET            /api/assignments/lecture10/assignment2                                    @io.training.api.controllers.AssignmentLectureTenController.assignment2(request: Request)
GET            /api/assignments/lecture10/assignment3                                    @io.training.api.controllers.AssignmentLectureTenController.assignment3(request: Request)
GET            /api/assignments/lecture10/assignment4                                    @io.training.api.controllers.AssignmentLectureTenController.assignment4(request: Request)
GET            /api/assignments/lecture10/assignment5                                    @io.training.api.controllers.AssignmentLectureTenController.assignment5(request: Request)
GET            /api/assignments/lecture10/assignment6                                    @io.training.api.controllers.AssignmentLectureTenController.assignment6(request: Request)`
const knightTourRoutes = `GET            /api/assignments/lecture10/knightTour           @io.training.api.controllers.AssignmentLectureTenController.knightTour(request: Request)`


const Card = (props) => {
  const { call, request, enabled, title, classes,  ...other } = props
  const { error, isLoading, response, runTests } = withTests([request], (input) => {
    const result = {
      endpoint: request.endpoint,
      options: {
        method: request.method,
      }
    }
    if (request.method === 'GET') {
      return result;
    }
    return {...result, options: {
        method: request.method,
        body: JSON.stringify(request.body),
      }}
  }, (input, output) => {
    return true
  }, (input, output) => {
    return output
  }, (input, error) => {
    return error.status === request.code
  }, enabled)

  const status = error ? error.status : !!response ? 200 : 0
  const isSet = status === request.code

  let display = 'Success'
  if (isLoading) {
    display = 'Loading'
  } else if (status !== request.code) {
    display = 'Not Correct, Retry?'
  }

  const onCopy = (event) => {
    event.preventDefault()
    event.stopPropagation()
    navigator.clipboard.writeText(JSON.stringify(request.sample, null, " "))
  }

  return <div {...other}>
    <Typography variant={'title'} className={classes.titleClass}>{title}</Typography>
    <LoadingIndicator show={isLoading} />
    {!isSet && <ErrorBox message={error && (error.message || 'Failed to contact the server! Make sure its on?')} onRetryClicked={onCopy} />}
    {!isSet && <Button className={classes.copy} onClick={onCopy}>Copy Response</Button>}
    <Chart className={classes.graphClass} options={response} />
  </div>
}

const Assignments = (props) => {
  const { classes, section } = props
  const pagination = section.children[0]
  const aggregations = section.children[1]
  const algorithm = section.children[2]

  const cardProps = {
    titleClass: classes.title,
    graphClass: classes.graph,
    copy: classes.copy
  }

  const categories = Array(40).fill(null).map(next => faker.commerce.productName())
  const brands = Array(10).fill(null).map(next => faker.commerce.productMaterial())

  const assignment5 = categories.map(next => {
    return {
      value: faker.random.float(),
      name: next
    }
  })

  const assignment5ExpectedData = () => {
    const pieCategories = categories.slice(0, 4).map(next => ({
      value: faker.random.float(),
      name: next
    }))
    const max = pieCategories.reduce((max, next) => Math.max(max, next.value), 0)
    return pieCategories.map(next => {
      const percentage = Math.max(next.value / max, 0.2)
      const emphasis = Math.min(percentage + 0.2, 1)
      return {...next, itemStyle: { 
        normal: { color: `rgb(60, 185, 226, ${percentage})`},
        emphasis: { color: `rgb(60, 185, 226, ${emphasis})`},
      }}
    })
  }
  const assignments = [
    {
      title: `Group data by 'categoryName', show sum(salesIncVatActual), order by 'categoryName' ascending`,
      request: {
        method: 'GET',
        endpoint: '/assignments/lecture10/assignment1',
        code: 200,
        sample: {
          xAxis: {
            type: 'category',
            data: categories,
          },
          yAxis: {
            type: 'value',
          },
          series: {
            type: 'line',
            areaStyle: {
              color: "#1b9ad1"
            },
            "color": "#293642",
            data: categories.map(next => {
              return {
                value: faker.random.float(),
                name: next
              }
            })
          }
        }
      }
    },
    {
      title: `Group data by 'brandName' and 'categoryName', show sum(salesIncVatActual), order by 'categoryName' ascending`,
      request: {
        method: 'GET',
        endpoint: '/assignments/lecture10/assignment2',
        code: 200,
        sample: {
          xAxis: {
            type: 'category',
            data: brands,
          },
          yAxis: {
            type: 'value',
          },
          series: categories.map((category) => ({
            type: 'bar',
            name: category,
            data: brands.map(next => ({
              value: faker.random.float(),
              name: next
            }))
          }))
        }
      }
    },
    {
      title: `Show data grouped on 'categoryName' by average 'salesIncVatActual', show top 10 based on their average sales`,
      request: {
        method: 'GET',
        endpoint: '/assignments/lecture10/assignment3',
        code: 200,
        sample: {
          series: {
            type: 'treemap',
            data: categories.slice(0, 10).map(next => {
              return {
                value: faker.random.float(),
                name: next
              }
            })
          }
        }
      }
    },
    {
      title: `Show data grouped on 'categoryName' by total 'volume', filter top 4 highest sold based on the sum salesIncVatActual. Color alpha (min 0.2) depends on the percentage of the slice! When hovered (emphasis) the alpha is increased by 0.2 (to a max of 1)`,
      request: {
        method: 'GET',
        endpoint: '/assignments/lecture10/assignment4',
        code: 200,
        sample: {
          series: {
            type: 'pie',
            data: assignment5ExpectedData()
          }
        }
      }
    },
    {
      title: `Compute total billable amount by multiplying volume with salesIncVatActual, show 'brandName' over this billable amount`,
      request: {
        method: 'GET',
        endpoint: '/assignments/lecture10/assignment5',
        code: 200,
        sample: {
          xAxis: {
            type: 'category',
            data: categories,
          },
          yAxis: {
            type: 'value',
          },
          visualMap: {
            top: 10,
            right: 10,
            min: assignment5.reduce((min, next) => Math.min(min, next.value), 0),
            max: assignment5.reduce((max, next) => Math.max(max, next.value), 0),
            type: "continuous"
          },
          series: {
            type: 'bar',
            data: assignment5
          }
        }
      }
    },
    {
      title: `Categorise data based on groups: '0-10', '10-100' and '100+' based on their salesIncVatActual grouped by brandName. The name of the group is the range for which they are grouped `,
      request: {
        method: 'GET',
        endpoint: '/assignments/lecture10/assignment6',
        code: 200,
        sample: {
          series: {
            type: 'treemap',
            data: ['0-10', '10-100', '100+'].map(next => {
              return {
                value: faker.random.float(),
                name: next
              }
            })
          }
        }
      }
    }
  ]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 10: {section.display}
        <Divider />
      </Typography>

      <Typography>
        For these assignments use the following route to fill in your database with data:
        <Code>
          {provided}
        </Code>
        Use the data located at /frontend/src/assets/data/sales_dummy_data.json<br/>

      </Typography>

      <Typography id={pagination.id} variant={'title'}>
        {pagination.display}
      </Typography>
      <Typography>
        Title: "Implement a fetch request with 20 items at a time (paginated)"<br/>
        Description: "A get request on the endpoint should return only 20 items at a time, sorted on most recent"<br/>
        Implement the following routes:
        <Code>
          {paginationRoutes}
        </Code>
      </Typography>
      <Typography variant="section">
        Tips
      </Typography>
      <Typography>
          The most recent item is that one which sorts descending on <Highlighted>_id</Highlighted>. In this case the token can be considered as the time on when the user started paginating. <Highlighted>token</Highlighted> is an ObjectId passed at the route as String.<br/>
          In other words, ObjectId embeds the time upon which an element was created. Using this information we can do two things:
          <ol>
            <li>Sort our items at the database based on their ObjectId (time of creation)</li>
            <li>Tell the time when the user started paginating (via ObjectId token)</li>
          </ol>
          For more information visit <SimpleLink href="https://docs.mongodb.com/manual/reference/method/ObjectId/">https://docs.mongodb.com/manual/reference/method/ObjectId/</SimpleLink>
        </Typography>

      <Typography id={aggregations.id} variant={'title'}>
        {aggregations.display}
      </Typography>
      <Typography>
        Title: "Implements and visualise Mathematical Functions using Mongo Aggregation Pipeline"<br/>
        Description: "Implement the graph functions according to definition"<br/>
        For more examples on available graphs take a look at: <SimpleLink href="https://ecomfe.github.io/echarts-examples/public/index.html">Echarts Demo</SimpleLink><br/>
        If you want to implement a new chart, then refer to the options: <SimpleLink href="https://ecomfe.github.io/echarts-doc/public/en/option.html">Echarts Graph Options</SimpleLink><br/>
        Implement the following routes:
        <Code>
          {graphRoutes}
        </Code>
      </Typography>
      <Typography variant="section">
        Tips
      </Typography>
      <Typography>
        <ol>
          <li>Open link: <SimpleLink href="https://echarts.apache.org/examples/en/editor.html?c=line-simple">https://echarts.apache.org/examples/en/editor.html?c=line-simple</SimpleLink></li>
          <li>Copy Sample Response on one of the cards</li>
          <li>Paste it at options = response</li>
        </ol>
      </Typography>
      <div className={classes.graphs}>
        {assignments.map((next, index) => (
          <Card
            key={index}
            className={classes.card}
            classes={cardProps}
            {...next}/>
        ))}
      </div>

      <Typography variant='title' id={algorithm.id}>
        {algorithm.display} Bonus!
      </Typography>
      <Typography variant='p'>
        Title: "Solve the Knights Tour Problem"<br/>
        Description: "Using Backtracking algorithm I am going to solve the Knights Tour Problem!"<br/>
        Here is the Explanation of the problem and a solution: <SimpleLink href="https://www.geeksforgeeks.org/the-knights-tour-problem-backtracking-1/">Knights Tour Problem</SimpleLink><br/>
      </Typography>
      <Typography variant='p'>
        Implement the solution at:
        <Code>
          {knightTourRoutes}
        </Code>
        <Typography fontStyle="italic">
          Use static data (your own) for Knights Tour problem.
        </Typography>
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(Assignments)
