/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import ReplicaSetImage from 'assets/images/lecture10/replica-set-read-write-operations-primary.bakedsvg.svg'
import ReplicaSetElection from 'assets/images/lecture10/replica-set-trigger-election.bakedsvg.svg'
import { Bold } from "presentations/Label";
import Code from "presentations/Code";

const styles = ({ size, typography }) => ({
  root: {
  }
})

const mongodCommand = `mongod --replSet training --port 27017 --bind_ip 127.0.0.1 --dbpath /usr/local/var/mongodb2  --oplogSize 128
mongod --replSet training --port 27018 --bind_ip 127.0.0.1 --dbpath /usr/local/var/mongodb3  --oplogSize 128
mongod --replSet training --port 27019 --bind_ip 127.0.0.1 --dbpath /usr/local/var/mongodb4  --oplogSize 128`

const rsInitiate = `rs.initiate({
   _id : "training",
   members: [
      { _id: 0, host: "127.0.0.1:27017" },
      { _id: 1, host: "127.0.0.1:27018" },
      { _id: 2, host: "127.0.0.1:27019" }
   ]
})`

const helperCommand = `db.adminCommand({shutdown : 1, force : true})`
const config = `mongo {
  host: "localhost,localhost,localhost"
  user: ""
  database: "training"
  password: ""
  auth_database: ""
  port = "27017,27018,27019"
}`

const Replication = (props) => {
  const { classes, section } = props
  const redundancy = section.children[0]
  const replicationInMongo = section.children[1]
  const failover = section.children[2]
  const deployment = section.children[3]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 10: {section.display}
        <Divider />
      </Typography>

      <Typography>
        A replica set in MongoDB is a group of <SimpleLink href="https://docs.mongodb.com/manual/reference/program/mongod/#bin.mongod">mongod</SimpleLink> processes that maintain the same data set. Replica sets provide redundancy and <SimpleLink href="https://docs.mongodb.com/manual/reference/glossary/#term-high-availability">high availability</SimpleLink>, and are the basis for all production deployments. This section introduces replication in MongoDB as well as the components and architecture of replica sets. The section also provides tutorials for common tasks related to replica sets.
      </Typography>

      <Typography id={redundancy.id} variant={'title'}>
        {redundancy.display}
      </Typography>
      <Typography>
        Replication provides redundancy and increases <SimpleLink href="https://docs.mongodb.com/manual/reference/glossary/#term-high-availability">data availability</SimpleLink>. With multiple copies of data on different database servers, replication provides a level of fault tolerance against the loss of a single database server.
      </Typography>
      <Typography>
        In some cases, replication can provide increased read capacity as clients can send read operations to different servers. Maintaining copies of data in different data centers can increase data locality and availability for distributed applications. You can also maintain additional copies for dedicated purposes, such as disaster recovery, reporting, or backup.
      </Typography>

      <Typography id={replicationInMongo.id} variant={'title'}>
        {replicationInMongo.display}
      </Typography>

      <Typography>
        A replica set is a group of <SimpleLink href="https://docs.mongodb.com/manual/reference/program/mongod/#bin.mongod">mongod</SimpleLink> instances that maintain the same data set. A replica set contains several data bearing nodes and optionally one arbiter node. Of the data bearing nodes, one and only one member is deemed the primary node, while the other nodes are deemed secondary nodes.
      </Typography>
      <Typography>
        The <SimpleLink href="https://docs.mongodb.com/manual/core/replica-set-primary/">primary node</SimpleLink> receives all write operations. A replica set can have only one primary capable of confirming writes with <SimpleLink href="https://docs.mongodb.com/manual/reference/write-concern/#writeconcern.%22majority%22">{`{w: "majority" }`}</SimpleLink> write concern; although in some circumstances, another mongod instance may transiently believe itself to also be primary. The primary records all changes to its data sets in its operation log, i.e. <SimpleLink href="https://docs.mongodb.com/manual/core/replica-set-oplog/">oplog</SimpleLink>. For more information on primary node operation, see <SimpleLink href="https://docs.mongodb.com/manual/core/replica-set-primary/">Replica Set Primary.</SimpleLink>
      </Typography>
      <img src={ReplicaSetImage} />
      <Typography>
        The <SimpleLink href="https://docs.mongodb.com/manual/core/replica-set-secondary/">secondaries</SimpleLink> replicate the primary’s oplog and apply the operations to their data sets such that the secondaries’ data sets reflect the primary’s data set. If the primary is unavailable, an eligible secondary will hold an election to elect itself the new primary. For more information on secondary members, see <SimpleLink href="https://docs.mongodb.com/manual/core/replica-set-secondary/">Replica Set Secondary Members</SimpleLink>.
      </Typography>

      <Typography id={failover.id} variant={'title'}>
        {failover.display}
      </Typography>
      <Typography>
        When a primary does not communicate with the other members of the set for more than the configured <Bold>electionTimeoutMillis</Bold> period (10 seconds by default), an eligible secondary calls for an election to nominate itself as the new primary. The cluster attempts to complete the election of a new primary and resume normal operations.
      </Typography>
      <img src={ReplicaSetElection}/>
      <Typography>
        The replica set cannot process write operations until the election completes successfully. The replica set can continue to serve read queries if such queries are configured to <SimpleLink href="https://docs.mongodb.com/manual/core/read-preference/#replica-set-read-preference">run on secondaries</SimpleLink> while the primary is offline.
      </Typography>

      <Typography>
        For more information visit:
        <ol>
          <li><SimpleLink href="https://docs.mongodb.com/manual/replication/">https://docs.mongodb.com/manual/replication/</SimpleLink></li>
        </ol>
      </Typography>
      <Typography id={deployment.id} variant={'title'}>
        {deployment.display}
      </Typography>

      <Typography>
        Let's look at how a replica server is deployed by using:
        <Code>
          {mongodCommand}
        </Code>
        And executing the following command at master:
        <Code>
          {rsInitiate}
        </Code>
        If you need to kill the running process first do:
        <Code>
          {helperCommand}
        </Code>
        Now our play app needs the following reference.config:
        <Code>
          {config}
        </Code>
        Make sure that the folders at dbPath exist before doing this!
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(Replication)
