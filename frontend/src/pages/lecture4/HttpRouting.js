/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Highlighted } from "presentations/Label";
import RouteErrorsPreview from 'assets/images/lecture4/route-errors-preview.png'
import Code from "presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})

const userById = `GET   /users/:id        @io.training.api.controllers.UsersController.getUserById(id: String)`
const routeWithComments = `# Returns a user using the id param
GET   /users/:id        @io.training.api.controllers.UsersController.getUserById(id: String)`

const corsFilter = `+ nocsrf
POST  /api/new              controllers.Api.newThing()`

const staticPath = `GET   /clients/all          controllers.Clients.list()`
const dynamicPath = `GET   /clients/:id          controllers.Clients.show(id: Long)`
const wildCard = `GET   /files/*name          controllers.Application.download(name)`
const regex = `GET   /items/$id<[0-9]+>    controllers.Items.show(id: Long)`
const routeMethodCall = `GET   /                     controllers.Application.homePage()`
const parametersRun = `# Extract the page parameter from the path.
# i.e. http://myserver.com/index
GET   /:page               @io.training.api.controllers.ApplicationController.show(page)`

const queryParamRun = `# Extract the page parameter from the query string.
# i.e. http://myserver.com/?page=index
GET   /                    @io.training.api.controllers.ApplicationController.show(page)`

const exampleHandler = `public CompletableFuture<Result> show(String page) {
  return CompletableFuture.supplyAsync(() -> ok(String.format("Got id %s", page)), ec.current());
}`

const dataTypeExample = `GET   /clients/:id          controllers.Clients.show(id: Long)`

const objectToJson = `public Result show(Long id) {
  Client client = clientService.findById(id);
  return ok(Json.toJson(client));
}`

const fixedValueRoutes = `# Extract the page parameter from the path, or fix the value for /
GET   /                     controllers.Application.show(page = "home")
GET   /:page                controllers.Application.show(page)`

const defaultValues = `# Pagination links, like /clients?page=3
GET   /clients              controllers.Clients.list(page: Int ?= 1)`

const optionalParameters = `# The version parameter is optional. E.g. /api/list-all?version=3.0
GET   /api/list-all         controllers.Api.list(version ?= null)
# or
GET   /api/list-all         controllers.Api.listOpt(version: java.util.Optional[String])`

const listParameters = `# The item parameter is a list.
# E.g. /api/list-items?item=red&item=new&item=slippers
GET   /api/list-items      controllers.Api.listItems(item: java.util.List[String])
# or
# E.g. /api/list-int-items?item=1&item=42
GET   /api/list-int-items  controllers.Api.listIntItems(item: java.util.List[Integer])`

const requestAsParam = `GET   /                     controllers.Application.dashboard(request: Request)`

const handlingRequestRoute = `public Result dashboard(Http.Request request) {
  return ok("Hello, your request path " + request.path());
}`

const reverseRoutingController = `package controllers;

import play.*;
import play.mvc.*;

public class Application extends Controller {

  public Result hello(String name) {
    return ok("Hello " + name + "!");
  }
}`
const reverseRouteExample = `# Hello action
GET   /hello/:name          controllers.Application.hello(name)`


const redirectReverse = `// Redirect to /hello/Bob
public Result index() {
  return redirect(controllers.routes.Application.hello("Bob"));
}`

class HttpRouting extends React.Component {
  render() {
    const { classes, section } = this.props
    const buildInRouter = section.children[0]
    const dependencyInjection = section.children[1]
    const routesSyntax = section.children[2]
    const httpMethod = section.children[3]
    const uriPattern = section.children[4]
    const callToAction = section.children[5]
    const routingPriority = section.children[6]
    const reverseRouting = section.children[7]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 4: {section.display}
          <Typography>
            Resources: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaRouting">https://www.playframework.com/documentation/2.8.x/JavaRouting</SimpleLink>
          </Typography>
          <Divider />
        </Typography>
        <Typography id={buildInRouter.id} variant={'title'}>
          {buildInRouter.display}
        </Typography>
        <Typography>
          The router is the component that translates each incoming HTTP request to an action call (a public method in a controller class).
        </Typography>
        <Typography>
          An HTTP request is seen as an event by the MVC framework. This event contains two major pieces of information:
          <ol>
            <li>the request path (such as <Highlighted>/clients/1542</Highlighted>, <Highlighted>/photos/list</Highlighted>), including the query string.</li>
            <li>the HTTP method (GET, POST, …).</li>
          </ol>
          Routes are defined in the conf/routes file, which is compiled. This means that you’ll see route errors directly in your browser:
        </Typography>
        <img src={RouteErrorsPreview}/>
        <Typography id={dependencyInjection.id} variant={'title'}>
          {dependencyInjection.display}
        </Typography>
        <Typography>
          Play’s default routes generator creates a router class that accepts controller instances in an @Inject-annotated constructor. That means the class is suitable for use with dependency injection and can also be instantiated manually using the constructor.
        </Typography>
        <Typography id={routesSyntax.id} variant={'title'}>
          {routesSyntax.display}
        </Typography>
        <Typography>
          <Highlighted>conf/routes</Highlighted> is the configuration file used by the router. This file lists all of the routes needed by the application. Each route consists of an HTTP method and URI pattern associated with a call to an action method.
        </Typography>
        <Typography>
          Let’s see what a route definition looks like:
          <Code>
            {userById}
          </Code>
          Each route starts with the HTTP method, followed by the URI pattern. The last element of a route is the call definition.
        </Typography>

        <Typography>
          You can also add comments to the route file, with the # character:
          <Code>{routeWithComments}</Code>
          It is also possible to apply modifiers by preceding the route with a line starting with a <Highlighted>+</Highlighted>. This can change the behavior of certain Play components. One such modifier is the “nocsrf” modifier to bypass the CSRF filter:
          <Code>{corsFilter}</Code>
        </Typography>
        <Typography id={httpMethod.id} variant={'title'}>
          {httpMethod.display}
        </Typography>
        <Typography>
          The HTTP method can be any of the valid methods supported by HTTP (
            <Highlighted>GET</Highlighted>,
            <Highlighted>PATCH</Highlighted>,
            <Highlighted>POST</Highlighted>,
            <Highlighted>PUT</Highlighted>,
            <Highlighted>DELETE</Highlighted>,
            <Highlighted>HEAD</Highlighted>,
            <Highlighted>OPTIONS</Highlighted>
          ).
        </Typography>
        <Typography id={uriPattern.id} variant={'title'}>
          {uriPattern.display}
        </Typography>
        <Typography>
          The URI pattern defines the route’s request path. Some parts of the request path can be dynamic.
        </Typography>
        <Typography variant='section'>
          Static path
        </Typography>
        <Typography>
          For example, to exactly match GET /clients/all incoming requests, you can define this route:
          <Code>
            {staticPath}
          </Code>
        </Typography>
        <Typography variant='section'>
          Dynamic parts
        </Typography>
        <Typography>
          If you want to define a route that, say, retrieves a client by id, you need to add a dynamic part:
          <Code>
            {dynamicPath}
          </Code>
          The default matching strategy for a dynamic part is defined by the regular expression <Highlighted>[^/]+</Highlighted>, meaning that any dynamic part defined as <Highlighted>:id</Highlighted> will match exactly one URI path segment. Unlike other pattern types, path segments are automatically URI-decoded in the route, before being passed to your controller, and encoded in the reverse route.
        </Typography>
        <Typography variant='section'>
          Dynamic parts spanning several /
        </Typography>
        <Typography>
          If you want a dynamic part to capture more than one URI path segment, separated by forward slashes, you can define a dynamic part using the <Highlighted>*id</Highlighted> syntax, also known as a wildcard pattern, which uses the <Highlighted>.*</Highlighted> regular expression:
          <Code>
            {wildCard}
          </Code>
          Here, for a request like GET <Highlighted>/files/images/logo.png</Highlighted>, the name dynamic part will capture the <Highlighted>images/logo.png</Highlighted> value.
        </Typography>
        <Typography fontStyle='italic'>
          Note that dynamic parts spanning several / are not decoded by the router or encoded by the reverse router. It is your responsibility to validate the raw URI segment as you would for any user input. The reverse router simply does a string concatenation, so you will need to make sure the resulting path is valid, and does not, for example, contain multiple leading slashes or non-ASCII characters.
        </Typography>

        <Typography variant='section'>
          Dynamic parts with custom regular expressions
        </Typography>
        <Typography>
          You can also define your own regular expression for a dynamic part, using the {`$id<regex>`} syntax:
          <Code>
            {regex}
          </Code>
        </Typography>
        <Typography fontStyle='italic'>
          Just like with wildcard routes, the parameter is not decoded by the router or encoded by the reverse router. You’re responsible for validating the input to make sure it makes sense in that context.
        </Typography>
        <Typography id={callToAction.id} variant={'title'}>
          {callToAction.display}
        </Typography>
        <Typography>
          The last part of a route definition is the call. This part must define a valid call to an action method.
        </Typography>
        <Typography>
          If the method does not define any parameters, just give the fully-qualified method name:
          <Code>
            {routeMethodCall}
          </Code>
          If the action method defines parameters, the corresponding parameter values will be searched for in the request URI, either extracted from the URI path itself, or from the query string.
          <Code>
            {parametersRun}
          </Code>
          Or:
          <Code>
            {queryParamRun}
          </Code>
          In both cases the data type of page is by default <Highlighted>String</Highlighted>
        </Typography>
        <Typography>
          Here is might be a corresponding show method definition in the controllers.Application controller:
          <Code>
            {exampleHandler}
          </Code>
        </Typography>
        <Typography variant="section">
          Parameter Types
        </Typography>
        <Typography>
          For parameters of type <Highlighted>String</Highlighted>, the parameter type is optional. If you want Play to transform the incoming parameter into a specific Scala type, you can add an explicit type:
          <Code>
            {dataTypeExample}
          </Code>
          Then use the same type for the corresponding action method parameter in the controller:
          <Code>
            {objectToJson}
          </Code>
          <Typography fontStyle="italic">
            Note: The parameter types are specified using a suffix syntax. Also, the generic types are specified using the [] symbols instead of {`<>`}, as in Java. For example, List[String] is the same type as the Java {`List<String>`}.
          </Typography>
        </Typography>
        <Typography variant="section">
          Parameters with fixed values
        </Typography>
        <Typography>
          Sometimes you’ll want to use a fixed value for a parameter:
          <Code>
            {fixedValueRoutes}
          </Code>
        </Typography>
        <Typography variant="section">
          Parameters with default values
        </Typography>
        <Typography>
          You can also provide a default value that will be used if no value is found in the incoming request:
          <Code>
            {defaultValues}
          </Code>
        </Typography>
        <Typography variant="section">
          Optional parameters
        </Typography>
        <Typography>
          You can also specify an optional parameter that does not need to be present in all requests:
          <Code>
            {optionalParameters}
          </Code>
        </Typography>
        <Typography variant="section">
          List parameters
        </Typography>
        <Typography>
          You can also specify list parameters for repeated query string parameters:
          <Code>
            {listParameters}
          </Code>
        </Typography>
        <Typography variant="section">
          Passing the current request to an action method
        </Typography>
        <Typography>
          You can also pass on the current request to an action method. Just add it as a parameter:
          <Code>
            {requestAsParam}
          </Code>
          And the corresponding action method:
          <Code>
            {handlingRequestRoute}
          </Code>
          Play will automatically detect a route param of type <Highlighted>Request</Highlighted> (which is an import for <Highlighted>play.mvc.Http.Request</Highlighted>) and will pass the actual request into the corresponding action method’s param. You can, of course, mix a <Highlighted>Request</Highlighted> param with other params and it doesn’t matter at which position the Request param is.
        </Typography>
        <Typography id={routingPriority.id} variant={'title'}>
          {routingPriority.display}
        </Typography>
        <Typography>
          Many routes can match the same request. If there is a conflict, the first route (in declaration order) is used.
        </Typography>
        <Typography id={reverseRouting.id} variant={'title'}>
          {reverseRouting.display}
        </Typography>
        <Typography>
          The router can be used to generate a URL from within a Java call. This makes it possible to centralize all your URI patterns in a single configuration file, so you can be more confident when refactoring your application.
        </Typography>
        <Typography>
          For each controller used in the routes file, the router will generate a ‘reverse controller’ in the routes package, having the same action methods, with the same signature, but returning a <Highlighted>play.mvc.Call</Highlighted> instead of a <Highlighted>play.mvc.Result</Highlighted>.
        </Typography>
        <Typography>
          The <Highlighted>play.mvc.Call</Highlighted> defines an HTTP call, and provides both the HTTP method and the URI.
        </Typography>
        <Typography>
          For example, if you create a controller like:
          <Code>
            {reverseRoutingController}
          </Code>
          And if you map it in the <Highlighted>conf/routes</Highlighted> file:
          <Code>
            {reverseRouteExample}
          </Code>
          You can then reverse the URL to the <Highlighted>hello</Highlighted> action method, by using the <Highlighted>controllers.routes.Application</Highlighted> reverse controller:
          <Code>
            {redirectReverse}
          </Code>
        </Typography>
        <Typography fontStyle='italic'>
          Note: There is a routes subpackage for each controller package. So the action controllers.Application.hello can be reversed via controllers.routes.Application.hello (as long as there is no other route before it in the routes file that happens to match the generated path).
        </Typography>
        <Typography>
          The reverse action method works quite simply: it takes your parameters and substitutes them back into the route pattern. In the case of path segments (:foo), the value is encoded before the substitution is done. For regex and wildcard patterns the string is substituted in raw form, since the value may span multiple segments. Make sure you escape those components as desired when passing them to the reverse route, and avoid passing unvalidated user input.
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(HttpRouting)
