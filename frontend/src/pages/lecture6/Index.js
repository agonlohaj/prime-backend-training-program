/**
 * Created by Agon Lohaj on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import Intro from "pages/lecture6/Intro";
import DataValidation from "pages/lecture6/DataValidation";
import DependencyInjection from "pages/lecture6/DependencyInjection";
import RedisCache from "pages/lecture6/RedisCache";
import Assignments from "pages/lecture6/Assignments";
import React from "react";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_6.VALIDATION:
        return <DataValidation {...props} />
      case PAGES.LECTURE_6.DEPENDENCY_INJECTION:
        return <DependencyInjection {...props} />
      case PAGES.LECTURE_6.REDIS_CACHING:
        return <RedisCache {...props} />
      case PAGES.LECTURE_6.ASSIGNMENTS:
        return <Assignments {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
