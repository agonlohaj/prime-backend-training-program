/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "../../presentations/rows/SimpleLink";
import { Bold, Highlighted } from "../../presentations/Label";
import Code from "../../presentations/Code";

const styles = ({ size, typography }) => ({
  root: {
  },
  img: {
    boxShadow: 'none'
  }
})

const aggregations = `db.orders.aggregate([
   { $match: { status: "A" } },
   { $group: { _id: "$cust_id", total: { $sum: "$amount" } } }
])`

const exercisesCode = `public Result aggregations (Http.Request request) {
  return ok(Json.newObject());
}`

const SimpleQueries = (props) => {
  const { classes, section } = props
  const pipeline = section.children[0]
  const singlePurpose = section.children[1]
  const exercises = section.children[2]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 9: {section.display}
        <Divider />
      </Typography>
      <Typography>
        <ol>
          <li><SimpleLink href="https://docs.mongodb.com/manual/aggregation/">https://docs.mongodb.com/manual/aggregation/</SimpleLink></li>
        </ol>
      </Typography>

      <Typography>
        Aggregation operations process data records and return computed results. Aggregation operations group values from multiple documents together, and can perform a variety of operations on the grouped data to return a single result. MongoDB provides three ways to perform aggregation: the aggregation pipeline, the map-reduce function, and single purpose aggregation methods.
      </Typography>

      <Typography fontStyle="italic">
        In this section we are going to focus on Aggregation Pipeline and Single Purpose Aggregation methods and what we can do with it
      </Typography>
      <Typography id={pipeline.id} variant={'title'}>
        {pipeline.display}
      </Typography>
      <Typography>
        MongoDB’s aggregation framework is modeled on the concept of data processing pipelines. Documents enter a multi-stage pipeline that transforms the documents into an aggregated result. For example:
      </Typography>
      <video width="400" controls>
        <source src="https://docs.mongodb.com/manual/_images/agg-pipeline.mp4" type="video/mp4"/>
          Your browser does not support HTML video.
      </video>
      <Typography>
        In the example,
        <Code>
          {aggregations}
        </Code>
        <ol>
          <li><Bold>First Stage</Bold>: The <SimpleLink href="https://docs.mongodb.com/manual/reference/operator/aggregation/match/#pipe._S_match">$match</SimpleLink> stage filters the documents by the status field and passes to the next stage those documents that have status equal to "A".</li>
          <li><Bold>Second Stage</Bold>: The $group stage groups the documents by the cust_id field to calculate the sum of the amount for each unique cust_id.</li>
        </ol>
      </Typography>
      <Typography>
        The MongoDB aggregation pipeline consists of <SimpleLink href="https://docs.mongodb.com/manual/reference/operator/aggregation-pipeline/#aggregation-pipeline-operator-reference">stages</SimpleLink>. Each stage transforms the documents as they pass through the pipeline. Pipeline stages do not need to produce one output document for every input document; e.g., some stages may generate new documents or filter out documents.
      </Typography>
      <Typography id={singlePurpose.id} variant={'title'}>
        {singlePurpose.display}
      </Typography>
      <Typography>
        MongoDB also provides <SimpleLink href="https://docs.mongodb.com/manual/reference/method/db.collection.estimatedDocumentCount/#db.collection.estimatedDocumentCount">db.collection.estimatedDocumentCount()</SimpleLink>, <SimpleLink href="https://docs.mongodb.com/manual/reference/method/db.collection.count/#db.collection.count">db.collection.count()</SimpleLink> and <SimpleLink href="https://docs.mongodb.com/manual/reference/method/db.collection.distinct/#db.collection.distinct">db.collection.distinct()</SimpleLink>.
      </Typography>

      <Typography id={exercises.id} variant={'title'}>
        {exercises.display}
      </Typography>

      <Typography>
        Let us get started with Aggregations by doing some exercises:
        <Code>
          {exercisesCode}
        </Code>
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(SimpleQueries)
