/**
 * Created by Agon Lohaj on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import Intro from "pages/lecture9/Intro";
import MongoDB from "pages/lecture9/MongoDB";
import SimpleQueries from "pages/lecture9/SimpleQueries";
import Aggregations from "pages/lecture9/Aggregations";
import Assignments from "pages/lecture9/Assignments";
import React from "react";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_9.MONGO_DB:
        return <MongoDB {...props} />
      case PAGES.LECTURE_9.MONGO_SIMPLE_QUERIES:
        return <SimpleQueries {...props} />
      case PAGES.LECTURE_9.AGGREGATIONS:
        return <Aggregations {...props} />
      case PAGES.LECTURE_9.ASSIGNMENTS:
        return <Assignments {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
